import requests

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def proxy(request, path, prefix):
    proxy_request = requests.Request(
        method=request.method, 
        url=prefix+path,
        data=getattr(request, request.method, None),
        files=request.FILES or None,
    ).prepare()
    proxy_session = requests.Session()
    proxy_response = proxy_session.send(proxy_request, stream=True)
    response = HttpResponse(
        proxy_response.raw.read(),
        content_type=proxy_response.headers.get("content-type", None),
    )
    return response
