django-rawproxy
===============

Proxy for Django

### Authors
*  Ondrej Sika, <http://ondrejsika.com>, ondrej@ondrejsika.com

### Source
* Python Package Index: <http://pypi.python.org/pypi/django-rawproxy>
* GitHub: <https://github.com/sikaondrej/django-rawproxy>

## Installation

install via pip

    pip install django-rawproxy
    

## Usage

add to your `urls.py`

    urlpatterns += patterns('',
        url(r'^map-api/(?P<path>.*)', "rawproxy.proxy", {
            "prefix": "http://maps.googleapi.com/"
        }),
    )
