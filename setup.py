#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "django-rawproxy",
    version = "1.0.0",
    url = 'https://github.com/sikaondrej/django-rawproxy/',
    download_url = 'https://github.com/sikaondrej/django-rawproxy/',
    license = 'MIT',
    author = "Ondrej Sika",
    aurhor_email = "ondrej@ondrejsika.com",
    description = "Django Proxy",
    py_modules = ["rawproxy", ],
    install_requires = ["requests"],
    include_package_data = True,
    zip_safe = False,
)
